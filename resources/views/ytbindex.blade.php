<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>YTB Api Log</title>

            <link href = {{ asset('css/app.css') }} rel="stylesheet" />
     </head>
  <body>
  	<div class="container mt-5">
    		<header>
          <img src="https://www.youtube.com/about/static/svgs/icons/brand-resources/YouTube-logo-full_color_light.svg?cache=72a5d9c" width="300">
    			
    		</header>

    		<section id="video">
    			
    		</section>

    		<main>
    			
    		</main>
  	</div>

  </body>
</html>